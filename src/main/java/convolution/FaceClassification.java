package convolution;

import javafx.application.Application;
import org.apache.commons.io.FilenameUtils;
import org.datavec.api.io.filters.BalancedPathFilter;
import org.datavec.api.io.labels.ParentPathLabelGenerator;
import org.datavec.api.split.FileSplit;
import org.datavec.api.split.InputSplit;
import org.datavec.image.loader.NativeImageLoader;
import org.datavec.image.recordreader.ImageRecordReader;
import org.datavec.image.transform.EqualizeHistTransform;
import org.datavec.image.transform.ImageTransform;
import org.datavec.image.transform.MultiImageTransform;
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator;
import org.deeplearning4j.earlystopping.EarlyStoppingConfiguration;
import org.deeplearning4j.earlystopping.EarlyStoppingModelSaver;
import org.deeplearning4j.earlystopping.EarlyStoppingResult;
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver;
import org.deeplearning4j.earlystopping.scorecalc.ScoreCalculator;
import org.deeplearning4j.earlystopping.termination.MaxEpochsTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxScoreIterationTerminationCondition;
import org.deeplearning4j.earlystopping.termination.MaxTimeIterationTerminationCondition;
import org.deeplearning4j.earlystopping.termination.ScoreImprovementEpochTerminationCondition;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.*;
import org.deeplearning4j.nn.conf.distribution.Distribution;
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution;
import org.deeplearning4j.nn.conf.distribution.NormalDistribution;
import org.deeplearning4j.nn.conf.inputs.InputType;
import org.deeplearning4j.nn.conf.inputs.InvalidInputTypeException;
import org.deeplearning4j.nn.conf.layers.*;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.api.IterationListener;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.deeplearning4j.parallelism.EarlyStoppingParallelTrainer;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.jita.conf.CudaEnvironment;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;
import org.nd4j.linalg.dataset.api.preprocessor.DataNormalization;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;


public class FaceClassification {
    protected static final Logger log = LoggerFactory.getLogger(FaceClassification.class);

    protected static String pathOfImageDirectories = System.getProperty("user.dir") + "/src/main/resources/faces/";
    protected static boolean overwriteSpecifiedParameters = true; //true -> program checks image dimensions, file and label count by itself
    protected static int height = 32; // 40
    protected static int width = 32; // 40

    protected static int channels = 1;
    protected static int numExamples = 59390; // no transformations
    protected static int numLabels = 50; // number of dirs

    protected static long seed = 22;
    protected static Random rng = new Random(seed);
    protected static int listenerFreq = 1;
    protected static int batchSize = 20;// 30; // max part of numExamples of one person processed at once
    protected static int iterations = 1; // 4
    protected static int maxEpochs = 250; // 200

    //early termination conditions
    protected static int evaluateEveryEpochs = 1 + maxEpochs/50;
    protected static int maxTrainingTime = 200;//45 in minutes
    protected static double maxScoreBeforeTermination = 1000; //stop if error exceeds this value
    protected static int maxEpochsWithNoImprovement = Math.max(1, maxEpochs/5);

    protected static int topN = 5; //for topN accuracy measure
    protected static double splitTrainTest = 0.75; //0.85
    protected static int nCores = 8;

    protected static boolean saveModel = true;
    private static String basePath = FilenameUtils.concat(System.getProperty("user.dir"), "src/main/resources/");
    private static String  writeModelPathString = basePath + "model.bin";
    private static String earlyStoppingModelSavePath = FilenameUtils.concat(System.getProperty("user.dir"), "EarlyStoppingBestModels/");
    private static String  metadataSavePath = earlyStoppingModelSavePath + "metadata.txt";
    private static File trainUiStatsFile = new File(earlyStoppingModelSavePath + "stats.log");

    protected int gpuParallelWorkers = 2; //Number of workers (executors * threads per executor) for the cluster
    protected int gpuPrefetchBuffer = 0; //Number of batches to asynchronously prefetch (0: disable)
    protected int gpuAveragingFrequency = 1; //Frequency (in number of minibatches) with which to average parameters

    protected static String modelType = "custom"; // LeNet, AlexNet or custom

    public MultiLayerNetwork customModel(){ //87% on 50 classes 32x32
        double nonZeroBias = 1;
        double dropOut = 1;
        double dropOutMiddle = 1;
        double dropOutLast = 0.3 ;
        int denseNeurons = 256;


        int layerNumber = 0;
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .iterations(iterations)
                .regularization(true).l2(0.0005) // 0.001 tried 0.0001, 0.0005
                .activation(Activation.SOFTSIGN)
                .learningRate(0.005) //  0.005 tried 0.00001, 0.00005, 0.000001
                .weightInit(WeightInit.RELU)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(Updater.RMSPROP)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerParamType)// normalize to prevent vanishing or exploding gradients
                .list()
                .layer(layerNumber, convInit("Warstwa konwolucyjna 1", channels, 32,  new int[]{7, 7}, new int[]{1, 1}, new int[]{0, 0}, 0, 0))
                .layer(++layerNumber, maxPool2x2("Warstwa podpróbkowania 1"))
                .layer(++layerNumber, conv("Warstwa konwolucyjna 2", 32, new int[]{5,5}, new int[]{1, 1}, new int[]{0, 0}, 0, 0))
                .layer(++layerNumber, maxPool2x2("Warstwa podpróbkowania 2"))
                .layer(++layerNumber, conv3x3("Warstwa konwolucyjna 3", 64, 0, dropOutMiddle))
                .layer(++layerNumber, maxPool2x2("Warstwa podpróbkowania 3"))
                .layer(++layerNumber, conv3x3("Warstwa konwolucyjna 4", 128, 0, 0))//220
                .layer(++layerNumber, maxPool2x2("Warstwa podpróbkowania 4"))
                .layer(++layerNumber, new DenseLayer.Builder().name("Warstwa gęsta").nOut(denseNeurons).dropOut(dropOut).build())
                .layer(++layerNumber, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .name("Warstwa wyjściowa")
                        .nOut(numLabels)
                        .activation(Activation.SOFTMAX)
                        .dropOut(dropOutLast)
                        .build())
                .backprop(true)
                //.backpropType(BackpropType.TruncatedBPTT)
                .pretrain(false)
                .setInputType(InputType.convolutional(height, width, channels))
                .build();

        return new MultiLayerNetwork(conf);
    }

    public void run(String[] args) throws Exception {
        // 2x less GPU ram used
        //up to 200% performance gains on memory-intensive operations, though the actual performance boost depends on the task and hardware used.
        //Place this call as the first line of your app, so that all subsequent allocations/calculations will be done using the HALF data type.
        //However you should be aware: HALF data type offers way smaller precision than FLOAT or DOUBLE, thus neural net tuning might become way harder.
        //DataTypeUtil.setDTypeForContext(DataBuffer.Type.HALF); //much faster, but 2x less precision
        //DataTypeUtil.setDTypeForContext(DataBuffer.Type.DOUBLE);

        // temp workaround for backend initialization
        CudaEnvironment.getInstance().getConfiguration()
                // key option enabled
                .allowMultiGPU(true)
                // we're allowing larger memory caches
                .setMaximumDeviceCache(3L * 1024L * 1024L * 1024L)
                // cross-device access is used for faster model averaging over pcie
                .allowCrossDeviceAccess(true);

        // This code will allow to cache up to 6GB of GPU RAM (it doesn’t mean that it WILL allocate that much though), and each individually cached memory chunk for both host and GPU memory might be up to 1GB in size.
      /*  CudaEnvironment.getInstance().getConfiguration()
                .setMaximumDeviceCacheableLength(1024 * 1024 * 1024L)
                .setMaximumDeviceCache(6L * 1024 * 1024 * 1024L)
                .setMaximumHostCacheableLength(1024 * 1024 * 1024L)
                .setMaximumHostCache(6L * 1024 * 1024 * 1024L);
     */

        System.out.println("Specified parameters:");
        System.out.println("iterations: " + iterations);
        System.out.println("epochs: " + maxEpochs);
        System.out.println("examples: " + numExamples);
        System.out.println("labels : " + numLabels);
        System.out.println("batch size: " + batchSize);
        System.out.printf("image height: %d, width: %d \n", height, width);

        if(overwriteSpecifiedParameters){
            boolean countRecursive = false;
            numLabels = getSubdirectoriesCount(pathOfImageDirectories, countRecursive);
            boolean countDirectories = false;
            numExamples = getFileCount(pathOfImageDirectories, countDirectories);

            System.out.println("Actual parameters:");
            System.out.println("iterations: " + iterations);
            System.out.println("epochs: " + maxEpochs);
            System.out.println("examples: " + numExamples);
            System.out.println("labels : " + numLabels);
            System.out.println("batch size: " + batchSize);
        }

        Map<String, Map<? extends Number, ? extends Number>> series = new HashMap<>(); //for CNNLearning class to visualise test accuracy
        Map<Integer, Double> trainAccuracyError = new HashMap<>(); //for CNNLearning class to visualise test accuracy
        Map<Integer, Double> testAccuracyError = new HashMap<>(); //for CNNLearning class to visualise test accuracy
        List<Double> trainAccuracyErrorList = new ArrayList<>(); //filled by score calculator
        List<Double> testAccuracyErrorList = new ArrayList<>(); //filled by score calculator



        log.info("Load data....");
        /**
         * Data Setup -> organize and limit data file paths:
         *  - mainPath = path to image files
         *  - fileSplit = define basic dataset split with limits on format
         *  - pathFilter = define additional file load filter to limit size and balance batch content
         **/
        ParentPathLabelGenerator labelMaker = new ParentPathLabelGenerator();
        File mainPath = new File(pathOfImageDirectories);
        FileSplit fileSplit = new FileSplit(mainPath, NativeImageLoader.ALLOWED_FORMATS, rng);
        //BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, batchSize);
        //BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, numExamples); //last was batchSize, but it makes with training only with this first batch
        BalancedPathFilter pathFilter = new BalancedPathFilter(rng, labelMaker, numExamples, numLabels, numExamples);// Math.min(numExamples, batchSize*40)); //last was batchSize, but it makes with training only with this first batch
        //System.out.println(pathFilter.toString());

        /**
         * Data Setup -> train test split
         *  - inputSplit = define train and test split
         **/
        InputSplit[] inputSplit = fileSplit.sample(pathFilter, splitTrainTest, 1 - splitTrainTest); // training and testing set division according to proportions (splitTrainTest)
        InputSplit trainData = inputSplit[0]; //whole train data set
        InputSplit testData = inputSplit[1]; //whole test data set
        System.out.println("train size: " + trainData.length());
        System.out.println("test size: " + testData.length());

        /*
        //for rotated images it may work incorrectly
        if(overwriteSpecifiedParameters) {
            int[] dimensions = getImageDimensionsFromFile(trainData.locations()[0]);
            width = dimensions[0];
            height = dimensions[1];
            System.out.printf("image height: %d, width: %d \n", height, width);
        }
        */

        /**
         * Data Setup -> transformation
         *  - Transform = how to tranform images and generate large dataset to train on
         **/
        //          org.datavec.image.transform. CropImageTransform FilterImageTransform ResizeImageTransform RotateImageTransform EqualizeHistTransform

//        ImageTransform colorTransform = new ColorConversionTransform(new Random(seed), COLOR_BGR2YCrCb);
        ImageTransform baseImageTransform = new MultiImageTransform(new Random(12345), new EqualizeHistTransform());
        List<ImageTransform> transforms = Arrays.asList(new ImageTransform[]{
                null, //use training data as it is
          /*      new FlipImageTransform(rng),
                new FlipImageTransform(new Random(1234)),
                //new WarpImageTransform(rng, 42),
                new RotateImageTransform(rng, 0, 0, 15, 1), //random/null_for_deterministic, max diff_x=0pixels x diff_y=0pixels for rotation center, max_rotation_angle=15deg., max_scale=1
                new EqualizeHistTransform(),
           */     //new ResizeImageTransform(40,40)
                /*new MultiImageTransform(new Random(1234),
                        baseImageTransform,
                        //new ScaleImageTransform(10) //in pixels
                        //new CropImageTransform(rng,(int)(0.05*height),(int)(0.05*width),(int)(0.05*height),(int)(0.05*width)),
                        new CropImageTransform(rng, (int) (0.08 * Math.min(width, height))),
                        new RotateImageTransform(rng, 2, 2, 15, 1), //random/null_for_deterministic, max diff_x=0pixels x diff_y=0pixels for rotation center, max_rotation_angle=15deg., max_scale=1
                        new FlipImageTransform(rng) //random: randomly; int: 0 Flips around x-axis.   >0 Flips around y-axis.   <0 Flips around both axes.
                )*/
        });
        /**
         *
         * Data Setup -> normalization
         *  - how to normalize images and generate large dataset to train on
         **/
        DataNormalization scaler = new ImagePreProcessingScaler(0, 1);

        log.info("Build model....");

        MultiLayerNetwork network;
        switch (modelType) {
            case "LeNet":
                network = lenetModel();
                break;
            case "AlexNet":
                network = alexnetModel();
                break;
            case "custom":
                network = customModel();
                break;
            default:
                throw new InvalidInputTypeException("Incorrect model provided.");
        }

        network.init();


/*
        //-------------DeepLearning4j UI
        //Initialize the user interface backend
        UIServer uiServer = UIServer.getInstance();
        uiServer.enableRemoteListener(); //Necessary: remote support is not enabled by default
        //Configure where the network information (gradients, score vs. time etc) is to be stored. Here: store in memory.
//        StatsStorage statsStorage = new InMemoryStatsStorage();         //Alternative: new FileStatsStorage(File), for saving and loading later
        StatsStorage statsStorage = new FileStatsStorage(trainUiStatsFile);
        //Alternative: new FileStatsStorage(File), for saving and loading later
        //Attach the StatsStorage instance to the UI: this allows the contents of the StatsStorage to be visualized
        uiServer.attach(statsStorage);
        //Then add the StatsListener to collect this information from the network, as it trains
        //--------------------

*/


        /**
         * Data Setup -> define how to load data into net:
         *  - recordReader = the reader that loads and converts image data pass in inputSplit to initialize
         *  - trainDataIter = a generator that only loads one batch at a time into memory to saveModel memory
         *  - trainIter = uses MultipleEpochsIterator to ensure model runs through the data for all epochs
         **/
        ImageRecordReader recordReader = new ImageRecordReader(height, width, channels, labelMaker);
        ImageRecordReader testRecordReader = new ImageRecordReader(height, width, channels, labelMaker);

        //create directory to saveModel model to
        File dirFile = new File(earlyStoppingModelSavePath);
        dirFile.mkdir(); // If mkdir fails, it is probably because the directory already exists. Which is fine.
        EarlyStoppingModelSaver<MultiLayerNetwork> saver = new LocalFileModelSaver(earlyStoppingModelSavePath);

        System.out.println("Training of the model...");
        log.info("Train model....");
        long tic = System.nanoTime();

        int transformIndex = 0;
        for (ImageTransform transform : transforms) {
            IterationListener trainScoreListener = new ScoreIterationListener(listenerFreq);
            //IterationListener timeIterationListener = new org.deeplearning4j.optimize.listeners.TimeIterationListener(listenerFreq);
            //IterationListener collectScoresIterationListener = new org.deeplearning4j.optimize.listeners.CollectScoresIterationListener(listenerFreq);

            //IterationListener statsListener = new StatsListener(statsStorage, listenerFreq);
            network.setListeners(trainScoreListener);//, statsListener);// timeIterationListener, collectScoresIterationListener);

            System.out.print("\nTraining on transformation: " + (Objects.isNull(transform) ? "no transformation" : (transformIndex+1) + " transformation: " + transform.getClass().toString()) + "\n");
            recordReader.initialize(trainData, transform);
            DataSetIterator trainDataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels); //it can iterate through trainData by taking batches
//            System.out.println("train examples: " + trainDataIter.numExamples());

            scaler.fit(trainDataIter);
            trainDataIter.setPreProcessor(scaler);
            //MultipleEpochsIterator trainIter = new MultipleEpochsIterator(epochs, trainDataIter, nCores);
            //network.fit(trainIter); //when EarlyStopping is not used

            //MultipleEpochsIterator trainIterEarlyStopping = new MultipleEpochsIterator(1, trainDataIter, nCores); //1 if early stopping is used

            //testRecordReader = new ImageRecordReader(height, width, channels, labelMaker);
            testRecordReader.initialize(testData, null); //TODO: null/transform; null to check for original test data; transform for transformed data
            //testDataIter = new RecordReaderDataSetIterator(testRecordReader, batchSize, (int)testData.length(), numLabels); //(int)... ->1
            DataSetIterator testDataIter = new RecordReaderDataSetIterator(testRecordReader, batchSize, 1, numLabels); //change batchSize to (int)testData.length() to test on the whole dataset at once but it gives the same score anyway //can iterate through testData by taking batchSize number of images at once
            // System.out.println("test examples: " + testDataIter.batch());
            scaler.fit(testDataIter);
            testDataIter.setPreProcessor(scaler);

            EarlyStoppingConfiguration <MultiLayerNetwork> esConf = new EarlyStoppingConfiguration.Builder<MultiLayerNetwork>()
                .epochTerminationConditions(
                        new MaxEpochsTerminationCondition(maxEpochs)
                        ,new ScoreImprovementEpochTerminationCondition(maxEpochsWithNoImprovement)
                )
                    .iterationTerminationConditions(
                            new MaxTimeIterationTerminationCondition(maxTrainingTime, TimeUnit.MINUTES)
                            ,new MaxScoreIterationTerminationCondition(maxScoreBeforeTermination)
                    )
                    //.scoreCalculator(new DataSetLossCalculator(testDataIter, true))
                    .scoreCalculator(new ScoreCalculator<MultiLayerNetwork>() {
                        @Override
                        public double calculateScore(MultiLayerNetwork model) {
                            Evaluation evalTrainOnEpoch = model.evaluate(trainDataIter, trainDataIter.getLabels(), topN);
                            Evaluation evalTestOnEpoch = model.evaluate(testDataIter, testDataIter.getLabels(), topN);
                            System.out.printf("****** model training data accuracy: %f and top-%d accuracy: %f ******\n", evalTrainOnEpoch.accuracy(), topN, evalTrainOnEpoch.topNAccuracy());
                            System.out.printf("****** model testing data accuracy: %f and top-%d accuracy: %f ******\n", evalTestOnEpoch.accuracy(), topN, evalTestOnEpoch.topNAccuracy());
                            trainAccuracyErrorList.add(1-evalTrainOnEpoch.accuracy() );
                            testAccuracyErrorList.add(1-evalTestOnEpoch.accuracy() );
                            return 1-evalTestOnEpoch.accuracy();
                        }
                    })
                    .evaluateEveryNEpochs(evaluateEveryEpochs)
                    //.saveLastModel()
                    .modelSaver(saveModel? saver : null) //.modelSaver(new LocalFileModelSaver(directory)); // TODO check if works with saveModel == false
                    .build();

            //EarlyStoppingTrainer trainer = new EarlyStoppingTrainer(esConf, network, trainIterEarlyStopping); //or simply trainDataIter if early stopping is not used
            EarlyStoppingParallelTrainer<MultiLayerNetwork> trainer = new EarlyStoppingParallelTrainer<>(esConf, network, trainDataIter, null, null, gpuParallelWorkers, gpuPrefetchBuffer, gpuAveragingFrequency); //or simply trainDataIter if early stopping is not used

            final int transformIndexF = transformIndex;

            /*
            EarlyStoppingListener earlyStoppingListener = new EarlyStoppingListener() {
                @Override
                public void onStart(EarlyStoppingConfiguration earlyStoppingConfiguration, Model model) {
                    System.out.println("Training begins on: " + LocalDateTime.now().toString() );
                    //System.out.println("S T A R T -----------------");
                    //firstScoreAtEpoch = earlyStoppingConfiguration.getScoreCalculator().calculateScore(model);
                    //System.out.println("Evaluated at the beginning of an epoch. Score: "+firstScoreAtEpoch);
                    //System.out.println("S T A R T -----------------");
                }
                @Override
                public void onEpoch(int i, double v, EarlyStoppingConfiguration earlyStoppingConfiguration, Model model) {
                    //System.out.println("E P O C H -----------------");
                    //System.out.println("i: " + i);
                    //System.out.println("v: " + v);
                    //System.out.println("ear " + earlyStoppingConfiguration.getIterationTerminationConditions().toString());
                    //System.out.println("ear " + earlyStoppingConfiguration.getEvaluateEveryNEpochs());
                    //System.out.println("score " + earlyStoppingConfiguration.getScoreCalculator().calculateScore(model));
                    //System.out.println("E P O C H -----------------");
                    System.out.printf("****** train score on epoch %d: %f, test score: %f=%f ******\n", i, model.score(), v, earlyStoppingConfiguration.getScoreCalculator().calculateScore(model));
                    Evaluation evalTrainOnEpoch = ((MultiLayerNetwork) model).evaluate(trainDataIter, trainDataIter.getLabels(), topN);
                    Evaluation evalTestOnEpoch = ((MultiLayerNetwork) model).evaluate(testDataIter, testDataIter.getLabels(), topN);
                    System.out.printf("****** model training data accuracy: %f and top-%d accuracy: %f ******\n", evalTrainOnEpoch.accuracy(), topN, evalTrainOnEpoch.topNAccuracy());
                    System.out.printf("****** model testing data accuracy: %f and top-%d accuracy: %f ******\n", evalTestOnEpoch.accuracy(), topN, evalTestOnEpoch.topNAccuracy());
                    trainAccuracyError.put(i, 1-evalTrainOnEpoch.accuracy() );
                    testAccuracyError.put(i, 1-evalTestOnEpoch.accuracy() );
                }

                @Override
                public void onCompletion(EarlyStoppingResult earlyStoppingResult) {
                    //System.out.println("C O M P L E T E D -----------------");
                    //System.out.println(earlyStoppingResult.getBestModelScore());
                    //System.out.println("C O M P L E T E D -----------------");
                    System.out.println("Training ended on: " + LocalDateTime.now().toString() );

                }
            };
            trainer.setListener(earlyStoppingListener); //trainData
            */


//Conduct early stopping training:
            EarlyStoppingResult result = trainer.fit();

//Print out the results:
            System.out.println("Termination reason: " + result.getTerminationReason());
            System.out.println("Termination details: " + result.getTerminationDetails());
            System.out.println("Total epochs: " + result.getTotalEpochs());
            System.out.println("Best epoch number: " + result.getBestModelEpoch());
            System.out.println("Score at best epoch: " + result.getBestModelScore());
            System.out.println("Score vs. epoch: " + result.getScoreVsEpoch().toString());
            Map<Integer, Double> scoreVsEpochUnedited = result.getScoreVsEpoch();

            Map<Integer,Double> scoreVsEpoch = new HashMap<>();
            //scoreVsEpoch.put(0, firstScoreAtEpoch);
            for(Integer k : scoreVsEpochUnedited.keySet()){
                if(!scoreVsEpochUnedited.get(k).isNaN()) {
                    scoreVsEpoch.put(k + 2, scoreVsEpochUnedited.get(k)); //because epoch number is counted from -1
                }
            }

            trainAccuracyError = IntStream.range(0, trainAccuracyErrorList.size()).boxed().collect(Collectors.toMap(i->i*evaluateEveryEpochs, trainAccuracyErrorList::get));
            testAccuracyError = IntStream.range(0, testAccuracyErrorList.size()).boxed().collect(Collectors.toMap(i->i*evaluateEveryEpochs, testAccuracyErrorList::get));

            //series.put("#"+transformIndex+ (Objects.isNull(transform)? " brak transformacji" : " transformacja" /*(" + transform.toString() + ")"*/ ), scoreVsEpoch);
            series.put("#"+(transformIndexF+1)+ " dane uczące", trainAccuracyError);
            series.put("#"+(transformIndexF+1)+ " dane testujące", testAccuracyError);

//Get the best model:
            network = (MultiLayerNetwork) result.getBestModel();
            ++transformIndex;

        }
        System.out.println("Training time: " + timeElapsed(tic));

        tic = System.nanoTime();
        log.info("Evaluate model....");

        //prepare data for testing purposes
         //data of batch size
//        recordReader.reset();
        recordReader.initialize(trainData);
        DataSetIterator trainDataIter = new RecordReaderDataSetIterator(recordReader, batchSize, 1, numLabels); //(int)trainData.length();
//        recordReader.initialize(trainData,  null);
        scaler.fit(trainDataIter);
        trainDataIter.setPreProcessor(scaler);

//        testRecordReader.reset();
        testRecordReader.initialize(testData);
        DataSetIterator testDataIter = new RecordReaderDataSetIterator(testRecordReader, batchSize, 1, numLabels); //(int)testData.length()
//        testRecordReader.initialize(testData,  null);
        scaler.fit(testDataIter);
        testDataIter.setPreProcessor(scaler);


        if(saveModel){
            saveMetadata(width, height, channels, trainDataIter.getLabels());
        }

        // Example on how to get predict results with trained model
        //Show predictions. Caution! Dont use whole set, but iterator with small batch, as it may not fit into RAM
        //DataSet trainDataSet = trainDataIter.next(); //NOT trainDataIterWhole!!!
        //DataSet testDataSet = testDataIter.next(); //NOT testDataIterWhole!!!
        //List<String> trainPredict = network.predict(trainDataSet);
        //List<String> testPredict = network.predict(testDataSet);
        //System.out.println("train actual: " + Arrays.stream(trainData.locations()).map(u -> new File(u).getName() ).collect(Collectors.joining(", ")) );
            //System.out.println("train actual size: " + trainData.locations().length );
        //System.out.println("train predict: " + trainPredict);
            //System.out.println("train predict size: " + trainPredict.size());

        //System.out.println("test actual: " + Arrays.stream(testData.locations()).map(u -> new File(u).getName() ).collect(Collectors.joining(", ")) );
            //System.out.println("test actual size: " + testData.locations().length );
        //System.out.println("test predict: " + testPredict);
            //System.out.println("test predict size: " + testPredict.size());


        Evaluation evalTrain = network.evaluate(trainDataIter, trainDataIter.getLabels(), topN);//top-N accuracy
        Evaluation evalTest = network.evaluate(testDataIter, testDataIter.getLabels(), topN);//top-N accuracy

        log.info(evalTrain.stats(true));
        log.info(evalTest.stats(true));
        //System.out.println( evalTrain.confusionToString() );
        //System.out.println( evalTrain.stats() );


        /*
        for(int i=0; i < testDataIter.getLabels().size(); i++) {
            System.out.printf("Summary for %d class (%s): \n", i, testDataIter.getLabels().get(i));
            System.out.printf("\ttrain precision: %f\n", evalTrain.precision(i));
            System.out.printf("\ttest precision: %f\n", evalTest.precision(i));
            System.out.printf("\trecall: %f\n", evalTrain.recall(i));
            System.out.printf("\tfalse positive rate: %f\n", evalTrain.falsePositiveRate(i));
            //System.out.printf("\tfalse negative rate: %f\n", evalTrain.falseNegativeRate(i));
            System.out.printf("\tf1 train score: %f\n", evalTrain.f1(i));
            System.out.printf("\tf1 test score: %f\n", evalTest.f1(i));
           // System.out.printf("all labels predicted as this class: %s\n", evalTrain.getPredictionByPredictedClass(i).toString());
           // System.out.printf("all labels that this class is classified as: %s\n", evalTrain.getPredictionsByActualClass(i).toString());
        }
        */
        System.out.println( "summary:\n" + network.summary() );
        System.out.println( "score:\n" + network.score() );


        //System.out.println(evalTrain.stats()); //very verbose
        //System.out.println(evalTest.stats()); //very verbose

        System.out.println( "confusion train:\n" + evalTrain.confusionToString() );
        System.out.println( "confusion test:\n" + evalTest.confusionToString() );



        System.out.println( "Train data scores:" );
        //Function <Map<?,Integer>, Integer> sumMapValues = map -> map.entrySet().stream().mapToInt(e->e.getValue()).sum();//reduce((sum, v)->sum+v).orElse(0);
        System.out.println( "\taccuracy train: " + evalTrain.accuracy() );
        System.out.println( "\tclassification error train: " + (1-evalTrain.accuracy()) );
        System.out.printf( "\ttrain: %f top-%d accuracy (%d/%d correct)\n", evalTrain.topNAccuracy(), topN, evalTrain.getTopNCorrectCount(), evalTrain.getTopNTotalCount());
        System.out.println( "\tfalse negatives: " + evalTrain.falseNegatives().toString() );
        //System.out.println( "\tfalse negative rate: " + (sumMapValues.apply(evalTrain.falseNegatives())/(double)trainData.length()) );// evalTrain.falseNegatives().entrySet().stream().map(e->e.getValue()).reduce((sum,v)->sum+v)) ;
        System.out.println( "\tfalse positives: " + evalTrain.falsePositives().toString() );
        System.out.println( "\tfalse positive rate: " + evalTrain.falsePositiveRate());
        //System.out.println( "\tfalse positive rate: " + (sumMapValues.apply(evalTrain.falsePositives())/(double)trainData.length()) );// evalTrain.falseNegatives().entrySet().stream().map(e->e.getValue()).reduce((sum,v)->sum+v)) ;
        System.out.println( "\tprecision: " + evalTrain.precision() );
        System.out.println( "\trecall: " + evalTrain.recall() );
        System.out.println( "\tf1 score: " + evalTrain.f1() );


        System.out.println( "Test data scores:" );
        //Function <Map<?,Integer>, Integer> sumMapValues = map -> map.entrySet().stream().mapToInt(e->e.getValue()).sum();//reduce((sum, v)->sum+v).orElse(0);
        System.out.println( "\taccuracy test: " + evalTest.accuracy() );
        System.out.println( "\tclassification error test: " + (1-evalTest.accuracy()) );
        System.out.printf( "\ttest: %f top-%d accuracy (%d/%d correct)\n", evalTest.topNAccuracy(), topN, evalTest.getTopNCorrectCount(), evalTest.getTopNTotalCount());
        System.out.println( "\tfalse negatives: " + evalTest.falseNegatives().toString() );
        //System.out.println( "\tfalse negative rate: " + (sumMapValues.apply(evalTest.falseNegatives())/(double)testData.length()) );// evalTest.falseNegatives().entrySet().stream().map(e->e.getValue()).reduce((sum,v)->sum+v)) ;
        System.out.println( "\tfalse positives: " + evalTest.falsePositives().toString() );
        System.out.println( "\tfalse positive rate: " + evalTest.falsePositiveRate());
        //System.out.println( "\tfalse positive rate: " + (sumMapValues.apply(evalTest.falsePositives())/(double)testData.length()) );// evalTest.falseNegatives().entrySet().stream().map(e->e.getValue()).reduce((sum,v)->sum+v)) ;
        System.out.println( "\tprecision: " + evalTest.precision() );
        System.out.println( "\trecall: " + evalTest.recall() );
        System.out.println( "\tf1 score: " + evalTest.f1() );

        System.out.println("Testing time: " +timeElapsed(tic) );



/*
        //another way of creating Evaluation object and evaluating the model
        // but it probably needs to load the whole data at once (without batches) to be able to test on whole data, so for big datasets it'll be impossible (not enough RAM)
        // often too much for evaluation to load whole
 //       recordReader.reset();
        recordReader.initialize(trainData);
        DataSetIterator trainDataIterWhole = new RecordReaderDataSetIterator(recordReader, (int)trainData.length(), 0, numLabels); //(int)trainData.length();
        scaler.fit(trainDataIterWhole);
        trainDataIterWhole.setPreProcessor(scaler);

 //       testRecordReader.reset();
        testRecordReader.initialize(testData);
        DataSetIterator testDataIterWhole = new RecordReaderDataSetIterator(testRecordReader, (int)testData.length(), 1, numLabels); //(int)testData.length()
        scaler.fit(testDataIterWhole);
        testDataIterWhole.setPreProcessor(scaler);

        Evaluation evaluationTest = new Evaluation(testDataIterWhole.getLabels(), topN);
        Evaluation evaluationTrain = new Evaluation(trainDataIterWhole.getLabels(), topN);
        INDArray outputTrain = network.output( trainDataSet.getFeatureMatrix() );
        INDArray outputTest = network.output( testDataSet.getFeatureMatrix() );
        evaluationTrain.eval(trainDataSet.getLabels(), outputTrain);
        evaluationTest.eval(testDataSet.getLabels(), outputTest);
        log.info("evaluation train stats: " + evaluationTrain.stats());
        log.info("evaluation test stats: " + evaluationTest.stats());

        System.out.println( "confusion train: \n" + evalTrain.confusionToString() );
        System.out.println( "confusion train (2): \n" + evaluationTrain.confusionToString() );
        System.out.println( "confusion test: \n" + evalTest.confusionToString() );
        System.out.println( "confusion test (2): \n" + evaluationTest.confusionToString() );
        System.out.println( "accuracy train: " + evalTrain.accuracy() );
        System.out.println( "accuracy train: (2)" + evaluationTrain.accuracy() );
        System.out.println( "accuracy test: " + evalTest.accuracy() );
        System.out.println( "accuracy test: (2)" + evaluationTest.accuracy() );
        System.out.println( "top-" + topN + " accuracy: " + evalTest.topNAccuracy() );
        System.out.println( "top-" + topN + " accuracy (2): " + evaluationTest.topNAccuracy() );

        System.out.println( "false negatives: " + evalTest.falseNegatives().toString() );
        System.out.println( "false positives: " + evalTest.falsePositives().toString() );
        System.out.println( "confusion matrix: \n" + evalTest.getConfusionMatrix().toString() );
        System.out.println( "precision: " + evalTest.precision() );
        System.out.println( "recall: " + evalTest.recall() );
        System.out.println( "f1: " + evalTest.f1() );



        System.out.printf( "train: %d/%d correct in top-%d\n", evalTrain.getTopNCorrectCount(), evalTrain.getTopNTotalCount(), topN);
        System.out.printf( "train: %d/%d correct in top-%d (2)\n", evaluationTrain.getTopNCorrectCount(), evaluationTrain.getTopNTotalCount(), topN);
        System.out.printf( "test: %d/%d correct in top-%d\n", evalTest.getTopNCorrectCount(), evalTest.getTopNTotalCount(), topN);
        System.out.printf( "test: %d/%d correct in top-%d (2)\n", evaluationTest.getTopNCorrectCount(), evaluationTest.getTopNTotalCount(), topN);
        System.out.println("Testing time: " +timeElapsed(tic) );

*/

        if (saveModel) {
            log.info("Save model....");
            ModelSerializer.writeModel(network, writeModelPathString, true);
        }

        //CNNLearning.setDataToPlot(series);
        //CNNLearning.setFigureLabels("Paweł Szczepanowski CNN", "Ocena jakości sieci konwolucyjnej", "Epoka ucząca","Błąd dla danych testujących");
        CNNLearning.setDataToPlot(series);
        CNNLearning.setFigureLabels("Paweł Szczepanowski CNN", "Ocena jakości sieci konwolucyjnej", "Epoka ucząca","Błąd predykcji");
        String[] argumentsToPlot = {};
        Application.launch(CNNLearning.class, argumentsToPlot);



        //TODO
       /*
        statsStorage.close();
        uiServer.stop();
        */




        log.info("****************Example finished********************");
    }

    private ConvolutionLayer convInit(String name, int in, int out, int[] kernel, int[] stride, int[] pad, double bias, double dropOut) {
        return new ConvolutionLayer.Builder(kernel, stride, pad).name(name).nIn(in).nOut(out).biasInit(bias).dropOut(dropOut).build();
    }

    private ConvolutionLayer conv(String name, int out, int[] kernel, int[] stride, int[] pad, double bias, double dropOut) {
        return new ConvolutionLayer.Builder(kernel, stride, pad).name(name).nOut(out).biasInit(bias).dropOut(dropOut).build();
    }

    private ConvolutionLayer conv1x1(String name, int out, double bias, double dropOut) {
        return conv(name, out, new int[]{1,1}, new int[]{1,1}, new int[]{1,1}, bias, dropOut );
    }

    private ConvolutionLayer conv3x3(String name, int out, double bias, double dropOut) {
        return conv(name, out, new int[]{3,3}, new int[]{1,1}, new int[]{1,1}, bias, dropOut );
    }

    private ConvolutionLayer conv5x5(String name, int out, int[] stride, int[] pad, double bias, double dropOut) {
        return conv(name, out, new int[]{5,5}, stride, pad, bias, dropOut);
    }


    private SubsamplingLayer maxPool(String name,  int[] kernel, int[] stride, int[] padding) {
        return new SubsamplingLayer.Builder(PoolingType.MAX, kernel, stride, padding).name(name).build();
    }
    private SubsamplingLayer maxPool2x2(String name) {
        //private SubsamplingLayer maxPool2x2(String name) {
        return maxPool(name, new int[]{2,2}, new int[]{1,1}, new int[]{0,0});
    }

    private SubsamplingLayer avgPool(String name,  int[] kernel, int[] stride, int[] padding) {
        return new SubsamplingLayer.Builder(PoolingType.AVG, kernel, stride, padding).name(name).build();
    }
    private SubsamplingLayer avgPool2x2(String name) {
        return avgPool(name,new int[]{2,2},new int[]{1,1}, new int[]{0,0});
    }

    private DenseLayer fullyConnected(String name, int out, double bias, double dropOut, Distribution dist) {
        return new DenseLayer.Builder().name(name).nOut(out).biasInit(bias).dropOut(dropOut).dist(dist).build();
    }

    public MultiLayerNetwork lenetModel() {
        /**
         * Revisde Lenet Model approach developed by ramgo2 achieves slightly above random
         * Reference: https://gist.github.com/ramgo2/833f12e92359a2da9e5c2fb6333351c5
         **/
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .iterations(iterations)
                .regularization(false).l2(0.005) // tried 0.0001, 0.0005
                .activation(Activation.RELU)
                .learningRate(0.01) // tried 0.00001, 0.00005, 0.000001
                .weightInit(WeightInit.XAVIER)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .updater(Updater.RMSPROP).momentum(0.9)
                .list()
                .layer(0, convInit("cnn1", channels, 50 ,  new int[]{5, 5}, new int[]{1, 1}, new int[]{0, 0}, 0, 0))
                .layer(1, maxPool2x2("maxpool1"))
                .layer(2, conv5x5("cnn2", 100, new int[]{5, 5}, new int[]{1, 1}, 0, 0))
                .layer(3, maxPool2x2("maxool2"))
                .layer(4, new DenseLayer.Builder().nOut(500).build())
                .layer(5, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .nOut(numLabels)
                        .activation(Activation.SOFTMAX)
                        .build())
                .backprop(true).pretrain(false)
                .setInputType(InputType.convolutional(height, width, channels))
                .build();

        return new MultiLayerNetwork(conf);

    }

    public MultiLayerNetwork alexnetModel() {
        /**
         * AlexNet model interpretation based on the original paper ImageNet Classification with Deep Convolutional Neural Networks
         * and the imagenetExample code referenced.
         * http://papers.nips.cc/paper/4824-imagenet-classification-with-deep-convolutional-neural-networks.pdf
         **/

        double nonZeroBias = 1;
        double dropOut = 0.5;

        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(seed)
                .weightInit(WeightInit.DISTRIBUTION)
                .dist(new NormalDistribution(0.0, 0.01))
                .activation(Activation.RELU)
                .updater(Updater.NESTEROVS)
                .iterations(iterations)
                .gradientNormalization(GradientNormalization.RenormalizeL2PerLayer) // normalize to prevent vanishing or exploding gradients
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(1e-2)
                .biasLearningRate(1e-2*2)
                .learningRateDecayPolicy(LearningRatePolicy.Step)
                .lrPolicyDecayRate(0.1)
                .lrPolicySteps(100000)
                .regularization(true)
                .l2(5 * 1e-4)
                .momentum(0.9)
                .miniBatch(false)
                .list()
                .layer(0, convInit("cnn1", channels, 96, new int[]{11, 11}, new int[]{4, 4}, new int[]{3, 3}, 0, 0))
                .layer(1, new LocalResponseNormalization.Builder().name("lrn1").build())
                .layer(2, maxPool("maxpool1", new int[]{3,3}, new int[]{1,1}, new int[]{0,0}))
                .layer(3, conv5x5("cnn2", 256, new int[] {1,1}, new int[] {2,2}, nonZeroBias, 0))
                .layer(4, new LocalResponseNormalization.Builder().name("lrn2").build())
                .layer(5, maxPool("maxpool2", new int[]{3,3},new int[]{1,1}, new int[]{0,0}))
                .layer(6,conv3x3("cnn3", 384, 0, 0))
                .layer(7,conv3x3("cnn4", 384, nonZeroBias, 0))
                .layer(8,conv3x3("cnn5", 256, nonZeroBias, 0))
                .layer(9, maxPool("maxpool3", new int[]{3,3}, new int[]{1,1}, new int[]{0,0}))
                .layer(10, fullyConnected("ffn1", 4096, nonZeroBias, dropOut, new GaussianDistribution(0, 0.005)))
                .layer(11, fullyConnected("ffn2", 4096, nonZeroBias, dropOut, new GaussianDistribution(0, 0.005)))
                .layer(12, new OutputLayer.Builder(LossFunctions.LossFunction.NEGATIVELOGLIKELIHOOD)
                        .name("output")
                        .nOut(numLabels)
                        .activation(Activation.SOFTMAX)
                        .build())
                .backprop(true)
                .pretrain(false)
                .setInputType(InputType.convolutional(height, width, channels))
                .build();

        return new MultiLayerNetwork(conf);

    }


    private static String timeElapsed(long tic){
        long elapsed = System.nanoTime() - tic;
        return elapsed/(long)10e8 + "s " + (elapsed % (long)10e8 / (long)10e5) + "ms";
    }

    private static int getSubdirectoriesCount(String dirPath, boolean countRecursive) {
        int count = 0;
        File f = new File(dirPath);
        File[] files = f.listFiles();
        if (files != null){
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.isDirectory()) {
                    count++;
                    if(countRecursive){
                        count += getSubdirectoriesCount(file.getAbsolutePath(), countRecursive);
                    }
                }
            }
        }
        return count;
    }

    private static int getFileCount(String dirPath, boolean countDirectories) {
        int count = 0;
        File f = new File(dirPath);
        File[] files = f.listFiles();
        if (files != null){
            for (int i = 0; i < files.length; i++) {
                count++;
                File file = files[i];
                if (file.isDirectory()) {
                    if(!countDirectories){
                        count--;
                    }
                    count += getFileCount(file.getAbsolutePath(), countDirectories);
                }
            }
        }
        return count;
    }

    private static int[] getImageDimensionsFromFile(URI uri) {
        BufferedImage bi;
        try {
            bi = ImageIO.read(new File(uri));
        } catch (IOException e) {
            return null;
        }
        return new int[]{ bi.getWidth(), bi.getHeight()};
    }

    /**
     *
     * @param width
     * @param height
     * @param channels
     * @param labels
     * @throws IOException
     *
     * file format:
     * width, height, channels
     * label1
     * label2
     * label3
     * ...
     */
    private static void saveMetadata(int width, int height, int channels, List<String> labels) throws IOException{
        String firstLine = String.format("%d,%d,%d",width,height,channels);
        Files.write(Paths.get(metadataSavePath),
        (Iterable<String>) Stream.concat( Arrays.asList(firstLine).stream(), labels.stream().map(String::trim)) ::iterator);
    }

    public static void main(String[] args) throws Exception {
        new FaceClassification().run(args);
    }

}
