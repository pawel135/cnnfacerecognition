package convolution;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;

import java.io.File;
import java.util.*;
import java.util.stream.*;



public class CNNLearning extends Application {
    private static Map<String, Map<? extends Number, ? extends Number>> dataToPlot;
    private static String windowTitle;
    private static String plotTitle;
    private static String xLabel;
    private static String yLabel;
    public static void setDataToPlot(Map<String, Map<? extends Number, ? extends Number>> dataToPlot){
      CNNLearning.dataToPlot = dataToPlot;
    }
    public static void setFigureLabels(String windowTitle, String plotTitle, String xLabel, String yLabel){
        CNNLearning.windowTitle = windowTitle;
        CNNLearning.plotTitle = plotTitle;
        CNNLearning.xLabel = xLabel;
        CNNLearning.yLabel = yLabel;
    }

    @Override public void start(Stage stage) {
        final NumberAxis xAxis = new NumberAxis();
        final NumberAxis yAxis = new NumberAxis();
        final LineChart<Number,Number> lineChart = new LineChart<>(xAxis,yAxis);
        stage.setTitle(windowTitle != null ? windowTitle : "");
        xAxis.setLabel(xLabel != null ? xLabel : "");
        yAxis.setLabel(yLabel != null ? yLabel : "");
        lineChart.setTitle(plotTitle != null ? plotTitle : "");

        XYChart.Series[] allSeries = dataToPlot == null ? new XYChart.Series[]{} : dataToPlot.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .map(m->{
                    XYChart.Series series = new XYChart.Series();
                    series.getData().addAll( m.getValue().entrySet().stream().map(e -> new XYChart.Data(e.getKey(), e.getValue())).collect(Collectors.toList()));
                    series.setName(m.getKey());
                    return series;
                })
                .toArray(XYChart.Series[]::new);
        Scene scene  = new Scene(lineChart,800,600);
        File f = new File("src/main/java/convolution/styles.css");
        scene.getStylesheets().clear();
        scene.getStylesheets().add("file:///" + f.getAbsolutePath().replace("\\", "/"));
        lineChart.getData().addAll(allSeries);//, series2, series3);
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
