Thesis project of image recognition based on CNN model. The trained network is based on existing solutions as well as one own network model is going to be proposed.

##Key points of the project:
* collecting images: both own set of images and part
* converting images to desired size and with desired parameters
* creating network model
* network learning based on the training data
* using image transformations to increase the number of available training data and support
* testing the model with different testing data
* outputting the results together with obtained score for the model

CNN model is based on features such as: maxpooling, convolution filters, RELU layers, fully connected layers.



